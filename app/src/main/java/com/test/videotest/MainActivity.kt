package com.test.videotest

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.content.pm.PackageManager
import android.media.projection.MediaProjectionManager
import android.net.Uri
import android.os.Bundle
import android.os.IBinder
import android.provider.Settings
import android.util.DisplayMetrics
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.test.videotest.ScreenCapturingService.RecordBinder
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    companion object {
        private const val PERMISSIONS_REQUEST_CODE = 100
        private const val RECORD_REQUEST_CODE = 101
        private const val CODE_DRAW_OVER_OTHER_APP_PERMISSION = 103
    }

    private lateinit var projectionManager: MediaProjectionManager
    private lateinit var recordService: ScreenCapturingService

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        projectionManager = getSystemService(MEDIA_PROJECTION_SERVICE) as MediaProjectionManager
        setContentView(R.layout.activity_main)

        checkAnRequestPermissions()

        if (Settings.canDrawOverlays(this)) {
            initButton()
        } else {
            val intent = Intent(
                Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                Uri.parse("package:$packageName")
            )
            startActivityForResult(intent, CODE_DRAW_OVER_OTHER_APP_PERMISSION)
        }
    }

    private fun checkAnRequestPermissions() {
        val permissions = arrayListOf<String>()
        if (ContextCompat.checkSelfPermission(
                this@MainActivity,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            )
            != PackageManager.PERMISSION_GRANTED
        ) {
            permissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE)
        }

        if (ContextCompat.checkSelfPermission(this@MainActivity, Manifest.permission.RECORD_AUDIO)
            != PackageManager.PERMISSION_GRANTED
        ) {
            permissions.add(Manifest.permission.RECORD_AUDIO)
        }

        val array = arrayOfNulls<String>(permissions.size)
        permissions.toArray(array)
        if (permissions.size > 0) {
            ActivityCompat.requestPermissions(
                this, array,
                PERMISSIONS_REQUEST_CODE
            )
        }
    }

    private fun initButton() {
        startButton.setOnClickListener { startRecording() }
        startButton.isEnabled = true
    }

    override fun onDestroy() {
        super.onDestroy()
        unbindService(connection)
    }


    private fun startRecording() {
        val intent = Intent(this, ScreenCapturingService::class.java)
        bindService(intent, connection, Context.BIND_AUTO_CREATE)
        val captureIntent = projectionManager.createScreenCaptureIntent()
        startActivityForResult(captureIntent, RECORD_REQUEST_CODE)
    }

    @SuppressLint("MissingSuperCall")
    override fun onActivityResult(
        requestCode: Int,
        resultCode: Int,
        data: Intent?
    ) {
        when {
            requestCode == RECORD_REQUEST_CODE && resultCode == Activity.RESULT_OK -> {
                val mediaProjection = projectionManager.getMediaProjection(resultCode, data!!)
                recordService.mediaProjection = mediaProjection
            }
            requestCode == RECORD_REQUEST_CODE && resultCode == Activity.RESULT_CANCELED ->{
                unbindService(connection)
                Toast.makeText(this, R.string.please_grant_permission, Toast.LENGTH_SHORT).show()
            }
            requestCode == CODE_DRAW_OVER_OTHER_APP_PERMISSION && Settings.canDrawOverlays(this) -> {
                initButton()
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == PERMISSIONS_REQUEST_CODE) {
            grantResults.forEach {
                if (it != PackageManager.PERMISSION_GRANTED) {
                    finish()
                }
            }
        }
    }

    private val connection: ServiceConnection = object : ServiceConnection {
        override fun onServiceConnected(className: ComponentName, service: IBinder) {
            val metrics = DisplayMetrics()
            windowManager.defaultDisplay.getMetrics(metrics)
            val binder = service as RecordBinder
            recordService = binder.getService()
            recordService.setConfig(metrics.widthPixels, metrics.heightPixels, metrics.densityDpi)
        }

        override fun onServiceDisconnected(arg0: ComponentName) {}
    }

}
