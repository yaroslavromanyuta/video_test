package com.test.videotest

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.Service
import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.PixelFormat
import android.hardware.display.DisplayManager
import android.hardware.display.VirtualDisplay
import android.media.MediaRecorder
import android.media.projection.MediaProjection
import android.os.*
import android.provider.MediaStore
import android.view.*
import android.widget.Button
import android.widget.ImageButton
import com.test.videotest.db.Record
import java.io.FileDescriptor
import java.io.IOException


class ScreenCapturingService : Service() {

    companion object {
        const val ONGOING_NOTIFICATION_ID = 1988
        const val MAX_VIDEO_DURATION_MS = 30 * 1000
    }

    private val windowManager: WindowManager by lazy {
        getSystemService(WINDOW_SERVICE) as WindowManager
    }
    private val buttonRootView: View by lazy {
        LayoutInflater.from(this).inflate(R.layout.overlay_button_layout, null)
    }
    private val params by lazy {
        WindowManager.LayoutParams(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY,
            WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
            PixelFormat.TRANSLUCENT
        )
    }

    var mediaProjection: MediaProjection? = null
    private lateinit var mediaRecorder: MediaRecorder
    private var virtualDisplay: VirtualDisplay? = null

    private var running = false
    private var width = 720
    private var height = 1080
    private var dpi = 0

    private var currentSessionName: String? = null
    private var currentRecordNumber = 0
    private var videosList: MutableList<String> = mutableListOf()

    private val dbDao by lazy { VideoCapturingApp.instance.database.recordDao() }
    private val startStopButton by lazy { buttonRootView.findViewById<Button>(R.id.startStopButton) }
    private val dragArea by lazy { buttonRootView.findViewById<ImageButton>(R.id.dragArea) }

    private val startStopTouchListener = object : View.OnTouchListener {
        private var initialX = 0
        private var initialY = 0
        private var initialTouchX = 0f
        private var initialTouchY = 0f

        override fun onTouch(v: View?, event: MotionEvent): Boolean {
            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    initialX = params.x
                    initialY = params.y
                    initialTouchX = event.rawX
                    initialTouchY = event.rawY
                    return false
                }
                MotionEvent.ACTION_UP -> {
                    return false
                }
                MotionEvent.ACTION_MOVE -> {
                    params.x = initialX + (event.rawX - initialTouchX).toInt()
                    params.y = initialY + (event.rawY - initialTouchY).toInt()
                    windowManager.updateViewLayout(buttonRootView, params)
                    return true
                }
            }
            return false
        }

    }

    private fun handleClick() {
        when (running) {
            true -> {
                stopRecord()
                startStopButton.setText(R.string.start_recording)
            }
            false -> {
                startRecord()
                startStopButton.setText(R.string.stop_recording)
            }
        }
    }

    override fun onBind(intent: Intent?): IBinder? {
        return RecordBinder()
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        return START_STICKY
    }

    override fun onCreate() {
        super.onCreate()
        val serviceThread = HandlerThread(
            "service_thread",
            Process.THREAD_PRIORITY_BACKGROUND
        )
        serviceThread.start()
        running = false
        mediaRecorder = MediaRecorder()
        showForegroundMessage()
        initOverLayButton()
    }

    private fun initOverLayButton() {
        addButtonOnTheScreen()
        setUpListeners()
    }

    private fun setUpListeners() {
        dragArea.setOnTouchListener(startStopTouchListener)
        startStopButton.setOnClickListener { handleClick() }
    }

    private fun addButtonOnTheScreen() {
        params.gravity = Gravity.TOP or Gravity.START
        params.x = 0
        params.y = 100

        windowManager.addView(buttonRootView, params)
    }

    override fun onDestroy() {
        super.onDestroy()
        stopRecord()
        windowManager.removeView(buttonRootView)
        virtualDisplay?.release()
        mediaProjection?.stop()
    }

    fun setConfig(width: Int, height: Int, dpi: Int) {
        this.width = width
        this.height = height
        this.dpi = dpi
    }

    @Synchronized
    fun startRecord(): Boolean {
        if (running) {
            return false
        }
        currentRecordNumber = 0
        currentSessionName = System.currentTimeMillis().toString()
        videosList.clear()
        showForegroundMessage()
        initRecorder()
        createVirtualDisplay()
        mediaRecorder.start()
        running = true
        return true
    }

    @Synchronized
    fun stopRecord(): Boolean {
        if (!running) {
            return false
        }
        running = false
        mediaRecorder.stop()
        mediaRecorder.reset()
        currentSessionName?.let {
            dbDao.insert(
                Record(name = it),
                videosList
            )
        }
        return true
    }

    @Synchronized
    fun restart() {
        mediaRecorder.stop()
        mediaRecorder.reset()
        initRecorder()
        createVirtualDisplay()
        mediaRecorder.start()
    }

    private fun createVirtualDisplay() {
        virtualDisplay = mediaProjection?.createVirtualDisplay(
            "MainScreen", width, height, dpi,
            DisplayManager.VIRTUAL_DISPLAY_FLAG_AUTO_MIRROR, mediaRecorder.surface, null, null
        )

    }

    private fun initRecorder() {
        val fileName = getFileName()
        videosList.add(fileName)
        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC)
        mediaRecorder.setVideoSource(MediaRecorder.VideoSource.SURFACE)
        mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP)
        mediaRecorder.setOutputFile(getFileDescriptor(fileName))
        mediaRecorder.setVideoSize(width, height)
        mediaRecorder.setVideoEncoder(MediaRecorder.VideoEncoder.H264)
        mediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB)
        mediaRecorder.setVideoEncodingBitRate(5 * 1024 * 1024)
        mediaRecorder.setVideoFrameRate(30)
        mediaRecorder.setMaxDuration(MAX_VIDEO_DURATION_MS)
        mediaRecorder.setOnInfoListener { _, what, _ ->
            when (what) {
                MediaRecorder.MEDIA_RECORDER_INFO_MAX_DURATION_REACHED -> {
                    currentRecordNumber++
                    restart()
                }
            }
        }

        try {
            mediaRecorder.prepare()
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

    private fun getFileDescriptor(name: String): FileDescriptor? {
        val values = ContentValues(5)
        val fileName = "${name}.mp4"
        values.put(MediaStore.Video.Media.TITLE, fileName)
        values.put(MediaStore.Video.Media.DISPLAY_NAME, fileName)
        values.put(
            MediaStore.Video.Media.DATE_ADDED,
            (System.currentTimeMillis() / 1000).toInt()
        )
        values.put(MediaStore.Video.Media.MIME_TYPE, "video/mp4")
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            values.put(MediaStore.Video.Media.RELATIVE_PATH, "DCIM/ScreenRecords/")
        }

        val uri = contentResolver.insert(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, values)
        return uri?.let { contentResolver.openFileDescriptor(it, "w")?.fileDescriptor }

    }

    private fun getFileName(): String =
        when (currentRecordNumber == 0) {
            true -> currentSessionName ?: System.currentTimeMillis().toString()
            false -> "$currentSessionName[$currentRecordNumber]"
        }

    private fun showForegroundMessage() {
        val channel = createNotificationChannel("screen_capturing_id", "ScreenCapturing")
        val notification = Notification.Builder(this, channel)
            .setSmallIcon(R.drawable.ic_launcher_foreground)
            .setContentTitle("Capturing video")
            .build()
        startForeground(ONGOING_NOTIFICATION_ID, notification)
    }

    private fun createNotificationChannel(channelId: String, channelName: String): String {
        val chan = NotificationChannel(
            channelId,
            channelName, NotificationManager.IMPORTANCE_NONE
        )
        chan.lightColor = Color.BLUE
        chan.lockscreenVisibility = Notification.VISIBILITY_PRIVATE
        val service = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        service.createNotificationChannel(chan)
        return channelId
    }

    inner class RecordBinder : Binder() {
        fun getService(): ScreenCapturingService = this@ScreenCapturingService
    }

}