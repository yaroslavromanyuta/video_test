package com.test.videotest.db

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = [Record::class, Video::class], version = 1)
abstract class RecordsDataBase: RoomDatabase() {
    abstract fun recordDao(): RecordDao
}