package com.test.videotest.db

import androidx.room.Embedded
import androidx.room.Relation

data class RecordWithVideos (
    @Embedded val record: Record,
    @Relation(
        parentColumn = "id",
        entityColumn = "recordId"
    )
    val videos: List<Video>
)