package com.test.videotest.db

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Video (
    val name: String,
    val recordId: Long
) {
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0
}