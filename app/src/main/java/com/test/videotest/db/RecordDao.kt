package com.test.videotest.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Transaction

@Dao
abstract class RecordDao {

    @Transaction
    open fun insert(record: Record, videoNames: List<String>){
        val recordId = insert(record)
        videoNames.forEach{
            insert(Video(it, recordId))
        }
    }

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    abstract fun insert(record: Record): Long

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    abstract fun insert(video: Video): Long
}