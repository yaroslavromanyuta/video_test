package com.test.videotest.db

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Record (
    var name: String
){
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0
}