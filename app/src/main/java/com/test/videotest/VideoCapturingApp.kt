package com.test.videotest

import android.app.Application
import androidx.room.Room
import com.facebook.stetho.Stetho
import com.test.videotest.db.RecordsDataBase


class VideoCapturingApp : Application() {

    companion object{
        @JvmStatic
        lateinit var instance: VideoCapturingApp
            private set
    }

    lateinit var database: RecordsDataBase

    override fun onCreate() {
        super.onCreate()
        instance = this
        database = Room.databaseBuilder<RecordsDataBase>(this, RecordsDataBase::class.java, "database")
            .allowMainThreadQueries()
            .build()

        Stetho.initializeWithDefaults(this)
    }
}